resource "monitor" "app-connetions" {
    name = "app-connections"
    metric = "Connections"
    target = aws_db_instance.app.id

    frequency = 60
    critical = {
        operator = ">="
        threshold = 85
        evaluation_periods = 5
    }

    warning = {
        operator = ">="
        threshold = 60
        evaluation_periods = 5
    }

    alarm_channel = "alarm SNS topic"
    ok_channel = "ok SNS topic"
}

resource "monitor" "app-cpu" {
    name = "app-cpu"
    metric = "CPUUtilization"
    target = aws_db_instance.app.id

    frequency = 60
    critical = {
        operator = ">="
        threshold = 80
        evaluation_periods = 5
    }

    warning = {
        operator = ">="
        threshold = 60
        evaluation_periods = 5
    }

    alarm_channel = "alarm SNS topic"
    ok_channel = "ok SNS topic"
}