# Dependencies
- Redis
- mongodb
- Rabbitmq

## How to get api token

- use the endpoint `api/v1/token` to get your api token and pass it in the header when sending a message


###  endpoints `/api/v1/`

| method | route             | description                  | data                                                    |
| ------ | ------------------|------------------------------|---------------------------------------------------------|
| POST   | /token            | Get api token                |  `{email}`                                              |
| POST   | /message          | Send message                 | `{message, task_id, contact_list}`                      |

## Tasks


### Containers

Check the controller directory for the Dockerfile 

- [ ] Looking at the Dockerfile, how can we avoid  busting the docker cache for the  dependencies every time the source code changes ?
- [ ] How can we reduce the size of the image generated from docker build ?
- [ ] Persist the database's data for future, so that when the service is stopped the data is not lost ?
- [ ] Automate all repetitive tasks such as building the image, starting/stopping the services ? (**bonus**)
- [ ] Test that everything works by accessing the [app](http://localhost:3004/) 

### Continuous Deployment

Using gitlab pipeline:

- [ ] Add the necessary steps to tag the image based on git commit hash or CI_JOB_ID ?
- [ ] Uploaded to AWS ECR using `0000000000.dkr.ecr.eu-west-3.amazonaws.com/app`  ?
- [ ] Update the image tag on the deployment manifest ?
- [ ] Add a step to deploy to Kubernetes ?
- [ ] How can you ensure that we don’t expose our AWS keys ?

### Infra with terraform

Looking at the `infra` directory

- [ ] How can we simplify the change of values that keep repeating ?
- [ ] How can we avoid duplicating some of the code if we are required to create 20 more databases ?


### Kubernetes

Looking at the `kubernetes` directory we can see the definition of a deployment and a service

- [ ] What kubectl command would you use to create an HorizontalPodAutoscaler with 1 minimum and 20 max replicas triggered by a cpu usage threshold of 70% ?
- [ ] Assuming the subdomain `app.com` how would you expose this application to the world ?
- [ ] Write the necessary configuration manifest to make it happen
